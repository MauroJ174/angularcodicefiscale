import { CodiceFiscalePage } from './app.po';

describe('codice-fiscale App', function() {
  let page: CodiceFiscalePage;

  beforeEach(() => {
    page = new CodiceFiscalePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
