import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { FormUserComponent } from './form-user/form-user.component';
import { UserComponent } from './user/user.component';
import {UserServiceService} from "./services/user-service.service";

@NgModule({
  declarations: [
    AppComponent,
    FormUserComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [UserServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
