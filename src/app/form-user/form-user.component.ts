import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {UserServiceService} from "../services/user-service.service";
import {User} from "../user/user";

@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.css']
})
export class FormUserComponent implements OnInit {
  user = new User();

  constructor(private service: UserServiceService) {
  }

  addUser(user) {
    this.service.setUser(user)
      .subscribe(result => {
        console.log(result);
      }, err => {
        console.log(err);
      })

  }

  ngOnInit() {
  }

}
