import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {User} from '../user/user';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UserServiceService {

  constructor(private http: Http) { }
  get getUser(): Observable<User> {
    return this.http
      .get('http://192.168.7.142:8080/user')
      .map((responseData) => responseData.json());
  }

  setUser (user: User): Observable<User> {
    console.log("eseguoset")
    console.log(user)
    return this.http
      .post('http://localhost:8080/user', user)
      .map((responseData) => responseData.json());
  }
}
