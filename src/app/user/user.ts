/**
 * Created by mauro on 05/05/2017.
 */

export class User {
  nome: String;
  cognome: String;
  g: number;
  m: number;
  a: number;
  sesso: boolean;
  comune: String;
  codiceFiscale: String;
}
